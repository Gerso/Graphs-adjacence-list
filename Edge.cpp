/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.cpp
 * Author: gerson
 * 
 * Created on 7 de Julho de 2018, 12:27
 */

#include "Edge.h"

Edge::Edge() {
}

Edge::Edge(const Edge& orig) {
}

Edge::~Edge() {
}

int Edge::GetCost() const {
    return cost;
}

void Edge::SetCost(int cost) {
    this->cost = cost;
}

bool Edge::IsDirected() const {
    return directed;
}

void Edge::SetDirected(bool directed) {
    this->directed = directed;
}

Vertex* Edge::GetVertex() const {
    return vertex;
}

void Edge::SetVertex(Vertex* vertex) {
    this->vertex = vertex;
}
