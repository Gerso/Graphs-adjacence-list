/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Graph.cpp
 * Author: gerson
 * 
 * Created on 8 de Julho de 2018, 13:31
 */

#include "Graph.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

Graph::Graph() {
}

Graph::Graph(const Graph& orig) {
}

Graph::~Graph() {
}

void Graph::GraphAL(int n) {
    this->edges = 0;
    this->V = n; // atribui o número de vértices
    adj = new list<Edge*>[n + 1]; // cria as listas
    vertices = new Vertex*[n];
    for (int i = 1; i <= n; i++) {
        Vertex *ver = new Vertex();
        ver->SetValue(i);
        std::stringstream ss;
        ss << i;
        string label = ss.str();
        ver->SetLabel(label);
        this->vertices[i] = ver;
    }
}

Vertex* Graph::GetVertex(int u) {
    return this->vertices[u];
}

void Graph::InsertEdge(int u, int v, bool dir, int c) {
    Edge *edge = HasEdge(u, v);
    if (edge == NULL) {
        Edge *aresta = new Edge();

        if (!dir) {

            Edge *aresta1 = new Edge();
            aresta1->SetCost(c);
            aresta1->SetDirected(dir);
            aresta1->SetVertex(GetVertex(u));
            adj[v].push_back(aresta1);

        }
        aresta->SetCost(c);
        aresta->SetDirected(dir);
        aresta->SetVertex(GetVertex(v));
        adj[u].push_back(aresta);

        this->edges++;
        cout << "█ Added edge \n";
    } else {
        cout << "█ Edge already exists\n";
    }
}

void Graph::PrintGraph() {
    for (int c = 1; c <= this->V; c++) {
        cout << "█" << c << " : ";
        for (list<Edge*>::iterator it = adj[c].begin(); it != adj[c].end(); ++it) {
            Edge *aresta = *it;

            if (adj[c].back() == aresta) {
                cout << aresta->GetVertex()->GetValue();
            } else {
                cout << aresta->GetVertex()->GetValue() << " -> ";
            }
        }
        cout << endl;
    }
}

int Graph::GetNVertices() {
    return this->V;
}

Edge* Graph::HasEdge(int u, int v) {
    for (list<Edge*>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
        Edge *a = *it;

        if (a->GetVertex()->GetValue() == v) {
            return a;
        }
    }
    return NULL;
}

bool Graph::IsDirected(int u, int v) {
    Edge *aresta = HasEdge(u, v);

    if (aresta != NULL) {
        return aresta->IsDirected();
    }
    return false;
}

bool Graph::IsEdgeValued(int u, int v) {
    Edge *aresta = HasEdge(u, v);

    if (aresta != NULL) {
        if (aresta->GetCost() == 0) {
            return false;
        } else {
            return true;
        }
    }
    return false;
}

int Graph::GetEdgeValue(int u, int v) {
    Edge *aresta = HasEdge(u, v);

    if (aresta != NULL) {
        return aresta->GetCost();
    }
    return 0;
}

int Graph::GetNEdges() {
    return this->edges;
}

void Graph::RemoveEdge(int u, int v) {
    Edge *edge = HasEdge(u, v);

    if (edge != NULL) {
        adj[u].erase(std::remove(adj[u].begin(), adj[u].end(), edge));
        this->edges--;
    } else {
        cout << "█ Not exist edge from "<< u  << "to " << v << "\n";
    }
    
    edge = HasEdge(v, u);

    if (edge != NULL) {
        adj[v].erase(std::remove(adj[v].begin(), adj[v].end(), edge));
        this->edges--;
    } else {
        cout << "█ Not exist edge from "<< v  << "to " << u << "\n";
    }

}

list<Vertex*> Graph::GetAdjacencyVertex(int v) {
    list<Vertex*> listVertex;
    for (list<Edge*>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        Edge * aresta = *it;
        listVertex.push_back(aresta->GetVertex());
    }
    return listVertex;
}

list<Edge*> Graph::GetIncidentEdges(int v) {
    list<Edge*> listAresta;
    for (list<Edge*>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        Edge * aresta = *it;
        listAresta.push_back(aresta);
    }
    return listAresta;
}

string Graph::GetVertexLabel(int v) {
    for (int c = 1; c <= this->V; c++) {
        if (vertices[c]->GetValue() == v) {
            return vertices[c]->GetLabel();
        }
    }
}

void Graph::SetVertexLabel(int v, string label) {
    for (int c = 1; c <= this->V; c++) {
        if (vertices[c]->GetValue() == v) {
            vertices[c]->SetLabel(label);
        }
    }
}