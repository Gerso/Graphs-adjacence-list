/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Grafo.cpp
 * Author: gerson
 * 
 * Created on 8 de Julho de 2018, 12:32
 */

// Grafos - Lista de adjacência

#include <iostream>
#include <list>
#include <algorithm>

#include "Edge.h"
#include "Graph.h"

using namespace std;

int menu() {
    system("clear");
    int option;
    cout << "██████████████████████████████████████████████████████████\n";
    cout << "█████████████████████  -  MENU  -  ███████████████████████\n";
    cout << "██████████████████████████████████████████████████████████\n";
    cout << "█                                                        █\n";
    cout << "█    1 - Insert             8  - Remove Edge             █\n";
    cout << "█    2 - Get Number Vertex  9  - Get Adjacency Vertex    █\n";
    cout << "█    3 - Has Edge           10 - Get Incident Edges      █\n";
    cout << "█    4 - Is Directed        11 - Set Vertex Label        █\n";
    cout << "█    5 - Is Edge Valued     12 - Get Vertex Label        █\n";
    cout << "█    6 - Get Edge Value     13 - Print Graph             █\n";
    cout << "█    7 - Get Number Edges   14 - Exit                    █\n";
    cout << "█                                                        █\n";
    cout << "██████████████████████████████████████████████████████████\n";
    cout << "█ option = ";
    cin >> option;
    return option;
}

int main() {
    int sizeVertex;
    int u, v, cost;
    char directed, exit = 'y';
    bool dir, valued;
    string newLabel;
    list<Vertex*> adjacentVertices;
    list<Vertex*>::iterator it;
    list<Edge*> adjacentEdges;
    list<Edge*>::iterator ite;
    Edge *edge;
    Graph graph;

    cout << "█ How many vertices do you want to create: ";
    cin >> sizeVertex;

    graph.GraphAL(sizeVertex);

    int option;
    while (exit != 'n' || exit != 'N') {
        option = menu();
        switch (option) {
            case 1:
                cout << "█ Enter the first vertex: ";
                cin >> u;
                cout << "█ Enter the second vertex: ";
                cin >> v;
                cout << "█ The edge is evaluated(t/f) : ";
                cin >> directed;
                cout << "█ Enter the cost of the edge: ";
                cin >> cost;

                if (directed == 't' || directed == 'T') {
                    graph.InsertEdge(u, v, true, cost);
                } else {
                    graph.InsertEdge(u, v, false, cost);
                }
                break;
            case 2:
                cout << "█ The graph has " << graph.GetNVertices() << " vertices\n";
                break;
            case 3:
                cout << "█ Enter the first vertex: ";
                cin >> u;
                cout << "█ Enter the second vertex: ";
                cin >> v;
                edge = graph.HasEdge(u, v);
                if (edge != NULL) {
                    cout << "█ There is an edge from vertex " << u << " to vertex " << v << endl;
                } else {
                    cout << "█ There is no edge from vertex " << u << " to vertex " << v << endl;
                }
                break;
            case 4:
                cout << "█ Enter the first vertex: ";
                cin >> u;
                cout << "█ Enter the second vertex: ";
                cin >> v;
                dir = graph.IsDirected(u, v);
                if (dir) {
                    cout << "█ The edge is directed \n";
                } else {
                    cout << "█ The edge is not directed \n";
                }
                break;
            case 5:
                cout << "█ Enter the first vertex: ";
                cin >> u;
                cout << "█ Enter the second vertex: ";
                cin >> v;
                valued = graph.IsEdgeValued(u, v);
                if (valued) {
                    cout << "█ The edge is valued \n";
                } else {
                    cout << "█ The edge is not valued \n";
                }
                break;
            case 6:
                cout << "█ Enter the first vertex: ";
                cin >> u;
                cout << "█ Enter the second vertex: ";
                cin >> v;
                cout << "█ The edge value is " << graph.GetEdgeValue(u, v) << endl;
                break;
            case 7:
                cout << "█ The graph has " << graph.GetNEdges() << " edges\n";
                break;
            case 8:
                cout << "█ Enter the first vertex: ";
                cin >> u;
                cout << "█ Enter the second vertex: ";
                cin >> v;
                graph.RemoveEdge(u, v);
                graph.PrintGraph();
                break;
            case 9:
                cout << "█ Enter the vertex: ";
                cin >> u;
                adjacentVertices = graph.GetAdjacencyVertex(u);
                cout << "█ " << u << " : ";
                
                for (it = adjacentVertices.begin(); it != adjacentVertices.end(); ++it) {
                    Vertex *vertex = *it;
                    if (adjacentVertices.back() == vertex) {
                        cout << vertex->GetValue() << "|" << vertex->GetLabel() << endl;
                    } else {
                        cout << vertex->GetValue() << "|" << vertex->GetLabel() << " -> ";
                    }
                }
                break;
            case 10:
                cout << "█ Enter the vertex: ";
                cin >> u;
                adjacentEdges = graph.GetIncidentEdges(u);
                cout << "█ " << u << " : ";
                
                for (ite = adjacentEdges.begin(); ite != adjacentEdges.end(); ++ite) {
                    Edge *aresta = *ite;
                    if (adjacentEdges.back() == aresta) {
                        cout << aresta->GetCost() << "|" << aresta->IsDirected() << endl;
                    } else {
                        cout << aresta->GetCost() << "|" << aresta->IsDirected() << " -> ";
                    }
                }
                break;
            case 11:
                cout << "█ Enter the vertex: ";
                cin >> u;
                cout << "█ Enter the name of the vertex: ";
                cin >> newLabel;
                graph.SetVertexLabel(u, newLabel);
                break;
            case 12:
                cout << "█ Enter the vertex: ";
                cin >> u;
                cout << "█ The Vertice " << u << " have the name: " << graph.GetVertexLabel(u) << endl;
                break;
            case 13:
                graph.PrintGraph();
                break;
            case 14:
                return 0;
              

        }
        cout << "█ Continue (y/n): ";
        cin >> exit;
        if (exit == 'n' || exit == 'N') {
            return 0;
        }
    }
    return 0;
}