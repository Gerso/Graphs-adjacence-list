/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Graph.h
 * Author: gerson
 *
 * Created on 8 de Julho de 2018, 13:31
 */

#ifndef GRAPH_H
#define GRAPH_H
#include <list>

#include "Edge.h"

using namespace std;

class Graph {
public:
    Graph();
    Graph(const Graph& orig);
    virtual ~Graph();
    
    /**
     * Class constructor, dynamically creates the vector that points to the lists
     * of adjacencies of the n vertices of the graph
     * @param n = numbers vertex
     */
    void GraphAL(int n);
    
    /**
     * Inserts the edge (u, v) into the graph structure if it does not yet
     * exists. The edge can be directed or not directed, this is defined by the parameter
     * dir. The parameter c informs the cost of the edge (u, v). Consider that if the edge is not
     * valued, its cost is zero;
     * @param u = vertice
     * @param v = vertice
     * @param dir = true or false
     * @param c = cost adge
     */
    void InsertEdge(int u,int v,bool dir,int c);
    
    /**
     * The structure of the graph, so that it is possible to understand
     * their vertices and how they relate from their edges.
     */
    void PrintGraph();
    
    /**
     * Returns the number of vertices in the graph
     * @return thi-> V
     */
    int GetNVertices();
    
    /**
     * Verifies if there is an edge (u, v), returns the edge if there is an edge in the graph
     * and null otherwise;
     * @param u = vertex
     * @param v = vertex
     * @return Edge or NULL
     */
    Edge* HasEdge(int u,int v);
    
    /**
     * Check if the edge (u, v) is a directed edge, returns true if positive, false otherwise;
     * @param u = vertex
     * @param v = vertex
     * @return true or false
     */
    bool IsDirected(int u,int v);
    
    /**
     * Verifies that the edge (u, v) is evaluated, returns true in case
     * positive, false otherwise;
     * @param u = vertex
     * @param v = vertex
     * @return true or false
     */
    bool IsEdgeValued(int u,int v);
    
    /**
     * Returns the value of the edge (u, v) if the edge exists
     * @param u
     * @param v
     * @return 
     */
    int GetEdgeValue(int u,int v);
    
    /**
     * Returns the value of the edge (u, v) if the edge exists
     * @return this->edges
     */
    int GetNEdges();
    
    /**
     * Removes the edge (u, v) of the graph structure;
     * @param u = vertex
     * @param v = vertex
     */
    void RemoveEdge(int u,int v);
    
    /**
     * Returns all vertices adjacent to vertex v
     * @param v = vertex
     * @return list<Vertex*> 
     */
    list<Vertex*> GetAdjacencyVertex(int v);
    
    /**
     * Returns all edges that have vertex v
     * @param v = vertex
     * @return list<Edge*>
     */
    list<Edge*> GetIncidentEdges(int v);
    
    /**
     * Defines a label for vertex v. By default, the vertex label is equal to its index
     * @param v = vertex
     * @param label = new label vertex
     */
    void SetVertexLabel(int v,string label);
    
    /**
     * Returns the label for vertex v
     * @param v = vertex
     * @return value of the vertex label
     */
    string GetVertexLabel(int v);
    
    /**
     * Returns the vertex at position u
     * @param u = vertex
     * @return vertex[u]
     */
    Vertex* GetVertex(int v);
    
    //to count the number of edges
    int edges;
private:
    int V; // number of vertices
    list<Edge*> *adj; // pointer to an array containing the adjacency lists
    Vertex **vertices; // array of vertices that stores the vertices of the graph
};

#endif /* GRAPH_H */

