/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.h
 * Author: gerson
 *
 * Created on 7 de Julho de 2018, 12:27
 */

#ifndef EDGE_H
#define EDGE_H

#include "Vertex.h"


class Edge {
public:
    Edge();
    Edge(const Edge& orig);
    virtual ~Edge();

    /**
     * Method to return the vertex that the edge connects
     * @return adjacence vertex 
     */
    Vertex* GetVertex() const;

    /**
     * Method to set the vertex that the edge connects
     * @param vertex
     */
    void SetVertex(Vertex *vertex);

    /**
     * @return edge cost  
     */
    int GetCost() const;
    
    /**
     * Method to assign the cost to an edge
     * @param cost
     */
    void SetCost(int cost);
    
    /**
     * @return true for directed edge or false to untargeted edge 
     */
    bool IsDirected() const;
    
    /**
     * Method to inform if an edge is directed or not
     * @param directed false or true
     */
    void SetDirected(bool directed);

private:
    //variable that represents the cost of the edge
    int cost;
    //variable that represents whether the edge is directed or not. if forwarded, it gets true if it does not receive false
    bool directed;
    //variable that represents the vertex that the edge connects
    Vertex *vertex;
};

#endif /* NODE_H */

