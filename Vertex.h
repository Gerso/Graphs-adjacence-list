/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Vertex.h
 * Author: gerson
 *
 * Created on 9 de Julho de 2018, 13:48
 */

#ifndef VERTEX_H
#define VERTEX_H

#include <string>

using namespace std;

class Vertex {
public:
    Vertex();
    Vertex(const Vertex& orig);
    virtual ~Vertex();

    int GetValue() const {
        return value;
    }

    void SetValue(int adjacence) {
        this->value = adjacence;
    }

    string GetLabel() const {
        return label;
    }

    void SetLabel(string label) {
        this->label = label;
    }

private:
    int value;
    string label;
};

#endif /* VERTEX_H */

